import React, { Fragment } from 'react';
import * as Icon from 'react-feather';

export default function Home() {
  return (
    <Fragment>
      <div className="container">
		    <div className="row justify-content-center">
		    	<div className="col">
		      	<div className="card">
		        	<div className="card-header">Quick Link</div>
		          <div className="card-body">
		          	<div className="row">
		          		<div className="col-sm-2">
		          			<button className="btn btn-link btn-lg"><Icon.ShoppingBag size={50} />
		          			<p><font color="black"> Barang </font></p></button>
		          		</div>
		          		<div className="col-sm-2">
		          			<button className="btn btn-link btn-lg"><Icon.Layers size={50} />
		          			<p><font color="black"> Absensi </font></p></button>
		          		</div>
		          		<div className="col-sm-2">
		          			<button className="btn btn-link btn-lg"><Icon.Users size={50} />
		          			<p><font color="black"> Karyawan </font></p></button>
		          		</div>
		          		<div className="col-sm-2">
		          			<button className="btn btn-link btn-lg"><Icon.Home size={50} />
		          			<p><font color="black"> Kasbon </font></p></button>
		          		</div>
		          		</div>
		          		</div>
		          	</div>
		          </div>
		          </div>
		        </div>
    </Fragment>
  )
}




