import React, { useState, useEffect, Fragment, Component } from 'react';
import { useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';
import { Table } from 'react-bootstrap';

import axios from 'axios';

export default function Desc() {
  const url = process.env.REACT_APP_API_KARYAWAN;
  const [data, setData] = useState([]);
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [halaman, setHalaman] = useState(null);


  useEffect(() => { 
    axios.get(`${url}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      },[page]);
  
  // viewHandler = async () => {
  //   axios(`${pdf}`, {
  //     method: "GET",
  //     responseType: "blob"
  //     //Force to receive data in a Blob Format
  //   })
  //     .then(response => {
  //       //Create a Blob from the PDF Stream
  //       const file = new Blob([response.data], {
  //         type: "application/pdf"
  //       });
  //       //Build a URL from the file
  //       const fileURL = URL.createObjectURL(file);
  //       //Open the URL on new Window
  //       window.open(fileURL);
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // };

  // const deleteList = (id) => {
  //   axios.delete(`${url}${id}`)
  //     .then(res => {
  //       // console.log(response);
  //       setData(data.filter(list => list.id !== id));
  //     })
  //     .catch(error => console.log(error))
  // }

  const deleteList = (id) => {
    console.log(id)
    axios.delete(`${url}${id}`)
      // console.log(response);
  }




  // const _delete = () => {

  //   if (window.confirm('Are you sure ?')) {
  //     deleteList(data.id);
  //   } else {
  //     console.log('cancel');
  //   }
  // }
  useEffect(() => { 
    axios.get(`${url}desc/?pageNumber=${page ?? '1'}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      }, [page]);

  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=1`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if (i === 1 || i === halaman.TotalPages || (i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${match.url}?page=${i}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=${halaman.TotalPages}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }

  return (
    <Fragment>
      <div className="container">
        <div className="row my-4">
          <div className="col">
            <h2>Data Karyawan</h2>
          </div>
          <div className="dropdown">
              <button type="button" className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort By</button>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                  <Link className="dropdown-item" to={`/asc`}>Sort By Asc</Link>
                  <Link className="dropdown-item" to={`/desc`}>Sort By Desc</Link>
                </div>
          </div>
          <div className="text-right">
            <Link className="btn btn-primary" to={`/tambahkaryawan`}> + Tambah</Link>
          </div>
            <Table striped bordered hover>
            <thead>
              <tr>
                <th> NIP </th>
                <th> Nama </th>
                <th> Alamat </th>
                <th> No. Telepon </th>
                <th> Email </th>
                <th colSpan="4">Action</th>
              </tr>
            </thead>
            <tbody>
            {data.map((listitem, index) => (
              <tr>
                <td>{listitem.nip}</td>
                <td>{listitem.nama}</td>
                <td>{listitem.alamat}</td>
                <td>{listitem.telp}</td>
                <td>{listitem.email}</td>
                <td><Link className="dropdown-item" to={`/editkaryawan/${listitem.id}`}>Edit</Link></td>
                <td><a className="dropdown-item" onClick={() => deleteList(listitem.id)} href="/karyawan">Delete</a></td>
              </tr>))}
            </tbody>
          </Table>
          <div> {pagination()} </div>
        </div>
      </div>
    </Fragment>
  )
}
