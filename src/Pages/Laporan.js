import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

export default function Laporan() {

return (              
	<div className="container">
		<div className="row my-2">
    	<div className="col">
      	<h2>Laporan</h2>
      </div>
    </div>
  <div className="row my-4 justify-content-center">
  	<div className="col">
    	<div className="card">
      	<div className="card-body rounded-lg">
        	<form autocomplete="off" className="mb-5">
          	<div className="form-group row pb-4">
            	<label className="col-lg-2 col-form-label">Jenis Laporan</label>
	            	<div className="col-lg-10">
		            	<select name="jenis_laporan" className="form-control" required>
			              <option value="">Pilih</option>
				            <option value="komisi_karyawan"> Komisi Karyawan </option>
				            <option value="gaji_karyawan"> Gaji Karyawan </option>
				            <option value="kinerja_karyawan">Kinerja Karyawan </option>
		              </select>
	            	</div>
            </div>
            <div className="form-group row pb-4">
              <label className="col-lg-2 col-form-label">Tanggal</label>
	              <div className="col-lg-10">
		              <div className="row">
			              <div className="col-4">
			                <input type="datetime-local" name="nama_barang" className="form-control" />
			              </div>
			              <div className="col-1">
			              	s/d
			              </div>
			              <div className="col-4">
			                <input type="datetime-local" name="nama_barang" className="form-control" />
			              </div>
	              </div>
           		</div>
            </div>
            <button type="submit" className="btn btn-primary">Lihat</button>
	        </form>
	       </div>
	      </div>
	    </div>
	  </div>
	</div>
	        
  )
}
