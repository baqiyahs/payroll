import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Header from './Components/Header';
import FormDataBarang from './Components/FormDataBarang';
import FormAbsensiK from './Components/FormAbsensiK';
import FormAbsensiM from './Components/FormAbsensiM';
import FormKasbon from './Components/FormKasbon';
import TabelBarang from './Components/TabelBarang';
import TabelAbsensiK from './Components/TabelAbsensiK';
import TabelKasbon from './Components/TabelKasbon';
import SideBar from './Components/SideBar';
import FormLogin from './Components/FormLogin';

import BarangEdit from './Container/BarangEdit';
import BarangLihat from './Container/BarangLihat';
import BarangTambah from './Container/BarangTambah';
import AbsenLihat from './Container/AbsenLihat';
import AbsenTambah from './Container/AbsenTambah';
import AbsenEdit from './Container/AbsenEdit';
import DataKaryawan from './Container/KaryawanLihat';
import KaryawanTambah from './Container/KaryawanTambah';
import KaryawanEdit from './Container/KaryawanEdit';
import Komisi from './Container/KomisiLihat';
import KomisiTambah from './Container/KomisiTambah';
import KomisiEdit from './Container/KomisiEdit';


import KasbonEdit from './Container/KasbonEdit';
import KasbonLihat from './Container/KasbonLihat';
import KasbonTambah from './Container/KasbonTambah';

import Home from './Pages/Home';
import Laporan from './Pages/Laporan';
import Asc from './Pages/AscKaryawan';
import Desc from './Pages/DescKaryawan';
import AscKomisi from './Pages/AscKomisi';
import DescKomisi from './Pages/DescKomisi';
import AscNamaBarang from './Pages/AscNamaBarang';
import DescNamaBarang from './Pages/DescNamaBarang';
import AscHargaBarang from './Pages/AscHargaBarang';
import DescHargaBarang from './Pages/DescHargaBarang';
import AscNominalKasbon from './Pages/AscNominalKasbon';
import DescNominalKasbon from './Pages/DescNominalKasbon';



function App() {
  return (
    <Router>
      <div className="wrapper">
        <Header />
        <main className="container-fluid py-4">
          <Switch>
            <Route exact path="/asc">
              <Asc />
            </Route>
            <Route exact path="/desc">
              <Desc />
            </Route>
            <Route exact path="/tambahkaryawan">
              <KaryawanTambah />
            </Route>
            <Route exact path="/karyawan">
              <DataKaryawan />
            </Route>
            <Route exact path="/editkaryawan/:id">
              <KaryawanEdit />
            </Route>
            <Route exact path="/tambahkomisi">
              <KomisiTambah />
            </Route>
            <Route exact path="/asckomisi">
              <AscKomisi />
            </Route>
            <Route exact path="/desckomisi">
              <DescKomisi />
            </Route>
            <Route exact path="/komisi">
              <Komisi />
            </Route>
            <Route exact path="/editkomisi/:id">
              <KomisiEdit />
            </Route>
            <Route exact path="/aschargabarang">
              <AscHargaBarang />
            </Route>
             <Route exact path="/deschargabarang">
              <DescHargaBarang />
            </Route>
            <Route exact path="/ascnamabarang">
              <AscNamaBarang />
            </Route>
             <Route exact path="/descnamabarang">
              <DescNamaBarang />
            </Route>
            <Route exact path="/tambahbarang">
              <BarangTambah />
            </Route>
            <Route path="/editbarang/:id">
              <BarangEdit />
            </Route>
            <Route exact path="/barang">
              <BarangLihat />
            </Route>
            <Route exact path="/descnominalkasbon">
              <DescNominalKasbon />
            </Route>
            <Route exact path="/ascnominalkasbon">
              <AscNominalKasbon />
            </Route>
            <Route exact path="/tambahkasbon">
              <KasbonTambah />
            </Route>
            <Route path="/editkasbon/:id">
              <KasbonEdit />
            </Route>
            <Route exact path="/kasbon">
              <KasbonLihat />
            </Route>
            <Route exact path="/tambahabsenmasuk">
              <AbsenTambah />
            </Route>
            <Route exact path="/editabsen/:id">
              <AbsenEdit />
            </Route>
            <Route exact path="/absen">
              <AbsenLihat />
            </Route>
            <Route exact path="/sidebar">
              <SideBar />
            </Route>
            <Route exact path="/laporan">
              <Laporan />
            </Route>
            <Route exact path="/coba">
              <FormLogin />*/}
            </Route>
            <Route exact path="/">
              <Home />
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
