import React, { Fragment, useState } from 'react';
import axios from 'axios';

import FormDataBarang from '../Components/FormDataBarang';

export default function BarangTambah() {
  const url = process.env.REACT_APP_API_DATABARANG;
  let initialState = {
    nama_barang: '',
    harga: '',
    komisi_karyawan: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'harga', 'komisi_karyawan') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <FormDataBarang handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}
