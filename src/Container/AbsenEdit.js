import React, { Fragment, useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';

import FormAbsensiK from '../Components/FormAbsensiK';

function AbsenEdit(props) {
  const url = process.env.REACT_APP_API_ABSENSI;
  let { id } = useParams();

  let initialState = {
    nama: '',
    jam_keluar: moment(new Date()).format(),
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    axios.get(`${url}${id}`)
      .then(result => setData(result.data))
  }, [id]);

  const handleInput = (event) => {
    const { name, value } = event.target;

    setData({ ...data, [name]: value });
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.put(`${url}Pulang/${id}`, data)
      .then(response => {
        console.log(response)
        setLoading(false);
        props.history.push('/');
      })
      .catch(error => console.log(error))
    
    setLoading(false);

  }

  return (
    <Fragment>
      <FormAbsensiK handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}

export default withRouter(AbsenEdit);