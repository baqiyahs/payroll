import React, { Fragment, useState } from 'react';
import axios from 'axios';

import FormDataKaryawan from '../Components/FormDataKaryawan';

export default function KaryawanTambah() {
  const url = process.env.REACT_APP_API_KARYAWAN;
  let initialState = {
    nip: '',
    nama: '',
    alamat: '',
    telp: '',
    email: '',
    password: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'nip') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <FormDataKaryawan handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}
