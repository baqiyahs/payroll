import React, { Fragment, useState } from 'react';
import axios from 'axios';

import FormKasbon from '../Components/FormKasbon';

export default function KasbonTambah() {
  const url = process.env.REACT_APP_API_KASBON;
  let initialState = {
    nama: '',
    tanggal: '',
    nominal: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'nominal') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <FormKasbon handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}
