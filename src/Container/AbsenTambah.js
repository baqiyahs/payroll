import React, { Fragment, useState } from 'react';
import axios from 'axios';
import moment from 'moment';

import FormAbsensiM from '../Components/FormAbsensiM';

export default function AbsenTambah() {
  const url = process.env.REACT_APP_API_ABSENSI;
  let initialState = {
    nama: '',
    jam_masuk: moment().format(),
    jam_keluar: moment().format(),
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;
      setData({...data, [name]: value})
    }

  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);


console.log(data)
 axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <FormAbsensiM handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}





