import React, { Fragment, useState } from 'react';
import axios from 'axios';
import FormDataKomisi from '../Components/FormDataKomisi';


export default function KomisiTambah() {
  const url = process.env.REACT_APP_API_KARYAWAN;
  let initialState = {
    nama: '',
    pekerjaan: '',
    tanggal: '',
    komisi: '',
    jumlah_pekerjaan: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'komisi', 'jumlah_pekerjaan') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <FormDataKomisi handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}
