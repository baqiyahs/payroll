import React, { Fragment, useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import axios from 'axios';

import FormDataBarang from '../Components/FormDataBarang';

function BarangEdit(props) {
  const url = process.env.REACT_APP_API_DATABARANG;
	let { id } = useParams();

	let initialState = {
    nama_barang: '',
    harga: '',
    komisi_karyawan: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  	axios.get(`${url}${id}`)
  		.then(result => setData(result.data))
  }, [id]);

  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'harga', 'komisi_karyawan') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.put(`${url}${id}`, data)
      .then(response => {
        console.log(response)
        setLoading(false);
        props.history.push('/');
      })
      .catch(error => console.log(error))
    
    setLoading(false);

  }

  return (
    <Fragment>
      <FormDataBarang handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}

export default withRouter(BarangEdit);