import React, { useState, useEffect, Fragment } from 'react';
import { useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';
import { Table } from 'react-bootstrap';
import * as Icon from 'react-feather';

import axios from 'axios';

export default function BarangLihat() {
  const url = process.env.REACT_APP_API_DATABARANG;
  const report = process.env.REACT_APP_API_REPORT;
  const [data, setData] = useState([]);
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [halaman, setHalaman] = useState(null);
  const [search, setSearch] = useState('');


  useEffect(() => { 
    axios.get(`${url}?pageNumber=${page ?? '1'}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      }, [page]);


  const deleteBarang = (id) => {
    console.log(id);
    axios.delete(`${url}${id}`)
      .then(response => {
         // console.log(error.response);
        setData(data.filter(list => list.id !== id));
      })
      .catch(error => console.log(id))
  }

  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=1`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if (i === 1 || i === halaman.TotalPages || (i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${match.url}?page=${i}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=${halaman.TotalPages}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }


  const handleSearch = (event) => {
       setSearch(event.target.value) 
    }

  const handleSubmit = (event) => {
       event.preventDefault()
       axios.get(`${url}Search/${search}`)
           .then (result => {
            setData(result.data)
            console.log(result)
           })
    }


  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
          <h2>Data Master Barang</h2>
        </div>
      </div>
    <div className="row my-4">
      <div className="col-8">
        <Link className="btn btn-primary" to={`/tambahbarang`}> + Tambah </Link>
        <a className="btn btn-primary" href={`${report}barang`}> Generate PDF </a>
      </div>
      <div className="col text-right">
        <form onSubmit={handleSubmit}>
          <div className="input-group">
            <input className="form-control" placeholder="Search" aria-label="Search" type="text" name="search" value={search} onChange={handleSearch}/>
            <button><Icon.Search color="black" size={20} /></button>
          </div>
        </form>
      </div>
    </div>
    <div className="col text-right">
      <div className="dropdown">
        <button type="button" className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Urut Berdasarkan</button>
          <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <Link className="dropdown-item" to={`/ascnamabarang`}>A - Z</Link>
            <Link className="dropdown-item" to={`/descnamabarang`}>Z - A</Link>
            <Link className="dropdown-item" to={`/aschargabarang`}>Harga Terendah - Harga Tertinggi </Link>
            <Link className="dropdown-item" to={`/deschargabarang`}>Harga Tertinggi - Harga Terendah</Link>
          </div>
      </div>
    </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card">
          <div className="card-body">
            <Table className="table table-hover">
              <thead>
                <tr>
                  <th> Kode Barang </th>
                  <th> Nama Barang </th>
                  <th> Harga </th>
                  <th> Komisi Karyawan </th>
                  <th colSpan="2" className="text-center"> Aksi </th>
                </tr>
            </thead>
            <tbody>
            {data.map((barangItem, index) => (
              <tr>
                <td>{barangItem.kode_barang}</td>
                <td>{barangItem.nama_barang}</td>
                <td>{barangItem.harga}</td>
                <td>{barangItem.komisi_karyawan}</td>
                <td><Link className="btn btn-default" to={`/editbarang/${barangItem.id}`}>Edit</Link></td>
                <td><a className="dropdown-item" href="/barang" onClick={() => deleteBarang(barangItem.id)}>Delete</a></td>
              </tr>))}
            </tbody>
        </Table>
      </div>
      </div>
      </div>
      </div>
      <div>{pagination()}</div>
      </div>
    </Fragment>
  )
}
