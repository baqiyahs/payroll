import React, { Fragment, useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import axios from 'axios';

import FormDataKomisi from '../Components/FormDataKomisi';

function KomisiEdit(props) {
  const url = process.env.REACT_APP_API_KOMISI;
	let { id } = useParams();

	let initialState = {
    nama: '',
    pekerjaan: '',
    tanggal: '',
    komisi: '',
    jumlah_pekerjaan: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  	axios.get(`${url}${id}`)
  		.then(result => setData(result.data))
  }, [id]);

  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'komisi', 'jumlah_pekerjaan') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.put(`${url}${id}`, data)
      .then(response => {
        console.log(response)
        setLoading(false);
        props.history.push('/');
      })
      .catch(error => console.log(error))
    
    setLoading(false);

  }

  return (
    <Fragment>
      <FormDataKomisi handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}

export default withRouter(KomisiEdit);