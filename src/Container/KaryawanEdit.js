import React, { Fragment, useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import axios from 'axios';

import FormDataKaryawan from '../Components/FormDataKaryawan';

function KaryawanEdit(props) {
  const url = process.env.REACT_APP_API_KARYAWAN;
	let { id } = useParams();

	let initialState = {
    nip: '',
    nama: '',
    alamat: '',
    telp: '',
    email: '',
    password: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  	axios.get(`${url}${id}`)
  		.then(result => setData(result.data))
  }, [id]);

  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'nip', 'telp') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.put(`${url}${id}`, data)
      .then(response => {
        console.log(response)
        setLoading(false);
        props.history.push('/');
      })
      .catch(error => console.log(error))
    
    setLoading(false);

  }

  return (
    <Fragment>
      <FormDataKaryawan handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}

export default withRouter(KaryawanEdit);