import React, { useState, useEffect, Fragment } from 'react';
import { useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';
import { Table } from 'react-bootstrap';
import * as Icon from 'react-feather';

import axios from 'axios';

export default function AbsenMasukLihat() {
  const url = process.env.REACT_APP_API_ABSENSI;
  const [data, setData] = useState([]);
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [halaman, setHalaman] = useState(null);
  const [search, setSearch] = useState('');


  useEffect(() => { 
    axios.get(`${url}?pageNumber=${page ?? '1'}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      }, [page]);


  const deleteAbsen = (id) => {
    console.log(id);
    axios.delete(`${url}${id}`)
      .then(response => {
         // console.log(error.response);
        setData(data.filter(list => list.id !== id));
      })
      .catch(error => console.log(id))
  }

  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=1`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if (i === 1 || i === halaman.TotalPages || (i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${match.url}?page=${i}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=${halaman.TotalPages}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }

  const handleSearch = (event) => {
       setSearch(event.target.value) 
    }

  const handleSubmit = (event) => {
       event.preventDefault()
       axios.get(`${url}Search/${search}`)
           .then (result => {
            setData(result.data)
            console.log(result)
           })
    }

  return (
    <Fragment>
    <div className="container">
      <div className="card">
        <div className="card-header">Quick Link</div>
        <div className="card-body">
          <div className="row">
            <div className="col-sm-4">
              <button className="btn btn-link btn-lg"><Icon.File size={50} />
              <p><font color="black"> Master Data Absen </font></p></button>
            </div>
            <div className="col-sm-4">
              <button className="btn btn-link btn-lg"><Icon.File size={50} />
              <p><font color="black"> Laporan </font></p></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row my-2">
        <div className="col">
            <h2>Data Absensi Karyawan</h2>
        </div>
      </div>
    <div className="row my-4">
      <div className="col-8">
        <Link className="btn btn-primary" to={`/tambahabsenmasuk`}> + Absen Masuk </Link>
      </div>
      <div className="col text-right">
        <form onSubmit={handleSubmit}>
          <div className="input-group">
            <input className="form-control" placeholder="Search" aria-label="Search" type="text" name="search" value={search} onChange={handleSearch}/>
            <button><Icon.Search color="black" size={20} /></button>
          </div>
        </form>
      </div>
    </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card">
          <div className="card-body">
            <Table className="table table-hover">
              <thead>
                <tr>
                  <th> Nama </th>
                  <th> Jam Masuk </th>
                  <th> Jam Keluar </th>
                  <th colSpan="3"> Aksi </th>
                </tr>
            </thead>
            <tbody>
            {data.map((absenItem, index) => (
              <tr>
                <td>{absenItem.nama}</td>
                <td>{absenItem.jam_masuk}</td>
                <td>{absenItem.jam_keluar}</td>
                <td><Link className="btn btn-primary" to={`/editabsen/${absenItem.id}`}>+ Absen Keluar </Link></td>
              </tr>))}
            </tbody>
        </Table>
        <div>{pagination()}</div>
      </div>
      </div>
      </div>
      </div>
      </div>
    </Fragment>
  )
}
