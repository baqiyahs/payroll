import React from 'react';
import { Link } from 'react-router-dom';

export default function Menu() {
  let listMenu = [
    {url: '/', title: 'Home'},
    {url: '/karyawan', title: 'Karyawan'},
    {url: '/komisi', title: 'Komisi'},
    {url: '/barang', title: 'Barang'},
    {url: '/absen', title: 'Absen'},
    {url: '/kasbon', title: 'Kasbon'},
    {url: '/laporan', title: 'Laporan'},
  ]

  return (
    <ul className="navbar-nav mr-auto flex-row">
      {listMenu.map((list, index) => (
        <li key={index} className="nav-item">
          <Link className="nav-link px-2" to={list.url}>{list.title}</Link>
        </li>
      ))}
    </ul>
  )
}