import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

export default function TabelBarang() {

return (
	<div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Data Master Barang</h2>
        </div>
      </div>
		<div className="row my-4 justify-content-center">
    	<div className="col">
      	<div className="card">
        	<div className="card-body">
        		<Table className="table table-hover">
	        		<thead>
								<tr>
									<th> Kode Barang </th>
									<th> Nama Barang </th>
									<th> Harga </th>
									<th> Komisi Karyawan </th>
									<th colSpan="3"> Aksi </th>
								</tr>
						</thead>
						<tbody>
		{/*{data.map((listitem, index) => (
      <tr>
	    	<td>{listitem.group_list}</td>
                <td><Link className="dropdown-item" to={`/editlist/${listitem.id}`}>Edit</Link></td>
                <td><a className="dropdown-item" href="/list" onClick={() => deleteList(listitem.id)}>Delete</a></td>
                <td><Link className="dropdown-item" to={`/group/${listitem.id}`}>Show Data</Link></td>
                <td><a className="dropdown-item" href={`${urlpdf}list/${listitem.id}`}>Generate PDF</a></td>	
      </tr>))}*/}
          </tbody>
        </Table>
      </div>
      </div>
      </div>
      </div>
      </div>
	)
}