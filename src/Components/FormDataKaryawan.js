import React, { Fragment } from 'react';

export default function FormDataKaryawan({handleSubmit, handleInput, data, loading}) {

  
  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Data Karyawan</h2>
        </div>
      </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card rounded-lg">
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">NIP</label>
                  <div className="col-lg-10">
                    <input type="number" name="nip" className="form-control" value={data.nip} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nama Karyawan</label>
                  <div className="col-lg-10">
                    <input type="text" name="nama" className="form-control" value={data.nama} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Alamat</label>
                  <div className="col-lg-10">
                    <input type="text" name="alamat" className="form-control" value={data.alamat} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">No Telepon</label>
                  <div className="col-lg-10">
                    <input type="number" name="telp" className="form-control" value={data.telp} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Email</label>
                  <div className="col-lg-10">
                    <input type="text" name="email" className="form-control" value={data.email} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Password</label>
                  <div className="col-lg-10">
                    <input type="text" name="password" className="form-control" value={data.password} onChange={handleInput} />
                  </div>
              </div>
              <div className="card-footer text-right">
                <button type="submit" className="btn btn-primary" disabled={loading}>Simpan</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}