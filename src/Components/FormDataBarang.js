import React, { Fragment } from 'react';
export default function FormDataBarang({handleSubmit, handleInput, data, loading, handleChange}) {

  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Data Barang</h2>
        </div>
      </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card rounded-lg">
          <form onSubmit={handleSubmit}>
            <div className="card-body">
             <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nama Barang</label>
                  <div className="col-lg-10">
                    <input type="text" name="nama_barang" className="form-control" value={data.nama_barang} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Harga</label>
                  <div className="col-lg-10">
                    <input type="number" name="harga" className="form-control" value={data.harga} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Komisi Karyawan</label>
                  <div className="col-lg-10">
                    <input type="number" name="komisi_karyawan" className="form-control" value={data.komisi_karyawan} onChange={handleInput} />
                  </div>
              </div>
              <div className="card-footer text-right">
                <button type="submit" className="btn btn-primary" disabled={loading}>Simpan</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}