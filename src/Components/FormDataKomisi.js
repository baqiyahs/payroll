import React, { Fragment } from 'react';

export default function FormDataKomisi({handleSubmit, handleInput, data, loading, handleChange}) {

  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Data Komisi</h2>
        </div>
      </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card rounded-lg">
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nama</label>
                  <div className="col-lg-10">
                    <input type="text" name="nama" className="form-control" value={data.nama} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Pekerjaan</label>
                  <div className="col-lg-10">
                    <input type="text" name="pekerjaan" className="form-control" value={data.pekerjaan} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Tanggal</label>
                  <div className="col-lg-10">
                    <input type="date" name="tanggal" className="form-control" value={data.tanggal} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Komisi</label>
                  <div className="col-lg-10">
                    <input type="number" name="komisi" className="form-control" value={data.komisi} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Jumlah Produksi</label>
                  <div className="col-lg-10">
                    <input type="number" name="jumlah_produksi" className="form-control" value={data.jumlah_produksi} onChange={handleInput} />
                  </div>
              </div>
              <div className="card-footer text-right">
                <button type="submit" className="btn btn-primary" disabled={loading}>Simpan</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}