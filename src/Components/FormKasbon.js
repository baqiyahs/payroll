import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';

export default function Form({handleSubmit, handleInput, data, loading, handleChange}) {
const url = process.env.REACT_APP_API_KARYAWAN;
  const [karyawan, setKaryawan] = useState([]);

  useEffect(() => {
    axios.get(`${url}`)
      .then(response => {
      setKaryawan(response.data);
      console.log(response.data);
      })  
      .catch(error => console.log(error))
      }, []);

  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Data Kasbon</h2>
        </div>
      </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card rounded-lg">
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nama</label>
                  <div className="col-lg-10">
                    <select className="form-control" id="sel1" name ="nama" onChange={handleInput}>
                      <option value="" disabled selected>Pilih</option>
                      {karyawan.map(karyawanItem => (
                      <option value={karyawanItem.nama}>{karyawanItem.nama}</option>))}
                    </select>
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Tanggal</label>
                  <div className="col-lg-10">
                    <input type="datetime-local" name="tanggal" className="form-control" value={data.tanggal} onChange={handleInput} />
                    
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nominal</label>
                  <div className="col-lg-10">
                    <input type="number" name="nominal" className="form-control" value={data.nominal} onChange={handleInput} />
                  </div>
              </div>
              <div class="card-footer text-right">
                <button type="submit" className="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}