import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';

export default function FormAbsensiK({handleSubmit, handleInput, data, loading, handleChange}) {
  const url = process.env.REACT_APP_API_KARYAWAN;
  const [karyawan, setKaryawan] = useState([]);
  const [startDate, setStartDate] = useState(new Date());

  useEffect(() => {
    axios.get(`${url}`)
      .then(response => {
      setKaryawan(response.data);
      console.log(response.data);
      })  
      .catch(error => console.log(error))
      }, []);


  return (
    <Fragment>
    <div className="container">
      <div className="row my-2">
        <div className="col">
            <h2>Absensi Karyawan Keluar</h2>
        </div>
      </div>
    <div className="row my-4 justify-content-center">
      <div className="col">
        <div className="card rounded-lg">
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Nama</label>
                  <div className="col-lg-10">
                    <input type="text" name="nama_barang" className="form-control" value={data.nama} onChange={handleInput} />
                  </div>
              </div>
              <div className="form-group row pb-4">
                <label className="col-lg-2 col-form-label">Jam Keluar</label>
                  <div className="col-lg-10">
                    <input className="form-control" type="datetime" name="jam_masuk" value={data.jam_keluar} disabled/>
                  </div>
              </div>
              <div class="card-footer text-right">
                <button type="submit" className="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}