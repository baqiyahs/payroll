import React, { Fragment } from 'react';

export default function Logo(props) {
  return (
    <Fragment>
      <a href='/' className="navbar-brand">{props.title}</a>
    </Fragment>
  )
}